//requiremnts
//Use at least one form (for the user to customise gamerTags and any other preferences/settings that may be relevant to your game (e.g. Sound on/off, dark theme, level selection, game mode, etc))

//Be comfortably playable on mobile

//Be published and be playable through a gitlab.io, bitbucket.io or similar for easy and accessible play.

//Compliant with PWA Standards meaning it can be installed for offline use through the web browser.


const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");
const gravity = 7;
const dpad = document.getElementById("dpad");
document .getElementById("canvas").style.display = "block";

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);

let isMode2 = false;
let gameMode = "mode1";
let showTextBox = true;


// Additional variables for background movement in mode 2
let mode2BackgroundOffset = 0;
let backgroundSpeed = 5;


document.addEventListener("keydown", function (event) {
    if (event.code === "Space") {
        // Jump when spacebar is pressed
        martyJump();
    }
});

//user info
// Handle form submission
document.getElementById('user-form').addEventListener('submit', function(event) {
    event.preventDefault();

    // Get the input values
    var name = document.getElementById('name').value;
    //var score = document.getElementById('score').value;
  
    // Store the values in localStorage
    localStorage.setItem('name', name);
   //localStorage.setItem('score', score);
  
    // Show the canvas
    var canvas = document.getElementById('canvas');
    canvas.style.display = 'block';
  
    // Hide the form
    var form = document.getElementById('user-form');
    form.style.display = 'none';
});

// Check if there are stored settings
var storedName = localStorage.getItem('name');
if (storedName) {
    // Ask the user if they want to restore their settings
    var confirmRestore = confirm('Do you want to restore your stored settings?');
  
    if (confirmRestore) {
        // Restore the stored settings
        // Update the player's name, score, etc. as needed
    } else {
        // Clear the stored settings
        localStorage.removeItem('name');
        // Clear other stored settings as needed
    }
}

//backgrounds
let gameMode1Background = "assets/img/SchoolYard.png";
let gameMode2Background = "assets/img/background.png";

function anmiateBackground2(){

    // Draw the background at an offset to create the looping effect
    context.drawImage(gameMode2Background, mode2BackgroundOffset, 0, canvas.width, canvas.height);
    context.drawImage(gameMode2Background, mode2BackgroundOffset + canvas.width, 0, canvas.width, canvas.height);

    // Update the offset for the next frame
    mode2BackgroundOffset -= 5; // Adjust the speed of the background movement
    if (mode2BackgroundOffset <= -canvas.width) {
        mode2BackgroundOffset = 0;
    }

}

//code for dark mode s
let isDarkMode = false;

themeToggle.addEventListener("change" , toggleTheme);

function toggleTheme() {
    isDarkMode = !isDarkMode;
    updateTheme();
}

function updateTheme() {
    if (isDarkMode){
        gameMode2Background.src = "assets/img/darkMode.png";
    } else {
        gameMode2Background.src = "assets/img/background.png";
    }
}

//Sprites/objects for game
//MARTY
let image = new Image();//lets image = 
image.src = "assets/img/Marty.png";

//initialising initial
let frames = 3;
let currentFrame = 0;

let initial = new Date().getTime();
let current;

//animate Marty
function PlayerAnimation() {
    current = new Date().getTime();
  
    if (current - initial >= 200) { // speed of animation
      currentFrame = (currentFrame + 1) % frames;
  
      initial = current;
    }
  
    // Scale factor for the player
    let scaleFactor = 5.0;
  
    // Calculate scaled width and height for the player
    let scaledWidth = image.width / 3 * scaleFactor;
    let scaledHeight = image.height * scaleFactor;
  
    // Draw the scaled player image
    context.drawImage(image, (image.width / 3) * currentFrame, 0, image.width / 3, image.height, marty.x, marty.y, scaledWidth, scaledHeight);
  }

//DOC
let image2 = new Image();
image2.src = "assets/img/Doc.png";

//LORRAINE
let image3 = new Image();
image3.src = "assets/img/Lorraine.png";

//lightening bolt
let image6 = new Image();
image6.src = "assets/img/lightening-bolt.png";

let framesL = 3;
let currentFrameL = 0;

let initialL = new Date().getTime();
let currentL;

//animated Lightening
function LighteningAnimation(){

    currentL = new Date().getTime();
    
    if(currentL - initialL >= 350){ //speed of animation
        currentFrameL = (currentFrameL + 1) % framesL;
    
    initialL = currentL;
}
context.drawImage(image6,(image6.width / 3) * currentFrameL,  0, image6.width / 3, image6.height, lightening.x, lightening.y, image6.width , image6.height);
}

//game object constructor
function GameObject(spritesheet, x, y, width, height) {//whats included ex height width etc
    this.spritesheet = spritesheet;//spritesheet=spritesheet
    this.x = x;//x=x
    this.y = y;//y=y
    this.width = width;//width=width
    this.height = height;//height=height
}

//positions
// marty
let marty = new GameObject(image, 90, 785, 100, 100);//x,y positions / height and width
let martyX = 90;
let martyY = 785;
marty.color = "white"; // Default color
// doc
//let scale = 0.5; // Set the scale factor (e.g., 0.5 for half the size)
let doc = new GameObject(image2, 500, 785, 200, 150);//x,y positions / height and width on new objects
// lorraine
//let scaleL = 0.5; // Set the scale factor (e.g., 0.5 for half the size)
let lorraine = new GameObject(image3, 1500, 900, 100, 150);//x,y positions / height and width on new objects
//lightening bolt
let lightening = new GameObject(image6, 1000, 700, 300, 200);//x,y positions / height and width on new objects

//jump function 
function martyJump() {
    if(marty.y === 800){
        marty.y -= 450; //  adjust the jump height by changing the value
    }   
}

//function to make marty come back down after jumping 
function applyGravity() {
    // Apply gravity to bring Marty back down
    if (marty.y < 800) {
        marty.y += gravity;//set a const for gravity 
    } else {
        marty.y = 800; // Ensure Marty stays on the ground
    }
}

//gamer input
function GamerInput(input){
    this.action = input;
}

let gamerInput = new GamerInput("none");//player inputs none

//input function 
function input(event) {
    //take input from player
    console.log(event);
    console.log("Event type: " + event.type);

    if(event.type === "keydown"){ //key down results in movement
       switch (event.key){
        case "ArrowUp":
            gamerInput = new GamerInput("Up");
            event.preventDefault();
            break;
        case "ArrowDown":
            gamerInput = new GamerInput("Down");
            event.preventDefault();
            break;
        case "ArrowRight":
            gamerInput = new GamerInput("Right");
            event.preventDefault();
            break;
        case "ArrowLeft":
                gamerInput = new GamerInput("Left");
                event.preventDefault();
                break;
            default:
                gamerInput = new GamerInput("None");
       }
    } else{
        gamerInput = new GamerInput("None");
    }

}


//update function 
function update() {
    if (gamerInput.action === "Up") {
       marty.y -= 10;//when player presses up key marty moves up 5 px
    } else if (gamerInput.action === "Down") {
        marty.y += 10;//when player presses down key marty moves down 5 px
    } else if (gamerInput.action === "Right") {
        marty.x += 10; // Move Marty forward by incrementing the x-coordinate
    } else if (gamerInput.action === "Left") {
        marty.x -= 10; // Move Marty forward by incrementing the x-coordinate

    }
  // Additional logic to handle Lorraine's movement
lorraine.x -= 5; // Adjust the speed of Lorraine's movement

if (lorraine.x + lorraine.width < 0) {
    // Lorraine has moved off the left edge, spawn her back on the right
    lorraine.x = canvas.width;
}
   
    if (marty.x >= canvas.width) {
        marty.x = 0;
        marty.y = 450;
        switchGameMode("mode2");

    }

    // Additional logic to handle Lorraine's movement after respawn in mode2
    if (gameMode === "mode2") {
        lorraine.x -= 5; // Adjust the speed of Lorraine's movement
        if (lorraine.x + lorraine.width < 0) {
            // Lorraine has moved off the left edge again, spawn her back on the right
            lorraine.x = canvas.width;
        }
    }
  
}


//switch mode function 
function switchGameMode(newMode) {
     gameMode = newMode;
}

// Function to start mode 2
function startMode2() {
    // Initialize mode 2
    initializeMode2();

    // Load assets for mode 2
    loadMode2Assets();

    // Set initial positions for mode 2
    setMode2InitialPositions();

    // Set the game mode to mode 2
    gameMode = "mode2";

}


// Load mode 2 assets
function loadMode2Assets() {
    //lorraine
   context.drawImage(image3, lorraine.x, lorraine.y, 200, 150);

    //lightening bolt
    context.drawImage(image6, lightening.x, lightening.y, 300, 200);
    LighteningAnimation();

    //movement
    image3.onload = function () {
        // Image has loaded, you can perform additional tasks if needed
    };
}
     //positions for mode 2 assets 
    function setMode2InitialPositions(){

        image3.x = 1500;
        image3.y = 500;
        image6.x = 1000;
        image6.y = 700;
    };
    

   //collision checking 
function isCollision(image, image3) {
    return (
     image.x < image3.x + image3.width &&
     image.x < image.width + image3.x &&
     image.y < image3.y + image3.height &&
     image.y < image.height + image3.y
    );
 }
   


 //dpad
document.getElementById("dpad-up").addEventListener("click", () => {
    gamerInput = new GamerInput("Up");
});

document.getElementById("dpad-down").addEventListener("click", () => {
    gamerInput = new GamerInput("Down");
});

document.getElementById("dpad-left").addEventListener("click", () => {
    gamerInput = new GamerInput("Left");
    player.x = "left";
});

document.getElementById("dpad-right").addEventListener("click", () => {
    gamerInput = new GamerInput("Right");
    player.x = "right";
});


// Draw function
function draw() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    if (gameMode === "mode1") {
        drawBackground(gameMode1Background);
    } else if (gameMode === "mode2") {
        // Draw the mode 2 background with movement
        drawBackgroundWithMovement(gameMode2Background);

        // Draw objects specific to mode 2
        context.drawImage(image3, lorraine.x, lorraine.y, 200, 150);
        LighteningAnimation();
    }

    // Update the offset for the next frame
    mode2BackgroundOffset -= backgroundSpeed;
    if (mode2BackgroundOffset <= -canvas.width) {
        mode2BackgroundOffset = 0;
    }

    // Draw common objects (e.g., player animation)
    PlayerAnimation();

    // Check for collision in mode 2
    if (gameMode === "mode2" && isCollision(marty, lorraine)) {
        // Collision detected, turn Marty red
        marty.color = "red";
    }

    // Draw objects based on game mode
    if (gameMode !== "mode2") {
        // The game mode is not 2, draw Doc
        context.drawImage(image2, 500, 785, 400, 350);
        // Draw the text box only in mode 1
        if (showTextBox) {
            context.fillStyle = "white";
            context.fillRect(490, 290, 900, 200);
            context.font = "bold 30px Arial";
            context.fillStyle = "black";
            context.fillText("Doc:", 500, 320);
            context.fillText("You must collect the lightning bolts for the time machine,", 500, 360);
            context.fillText("but avoid your mother as it may mess with your existence", 500, 400);
            context.fillText("PRESS AND HOLD RIGHT ARROW TO CONTINUE -->", 500, 455);
        }
    }
}


// Function to draw the background with movement
function drawBackgroundWithMovement(backgroundImage) {
    let image = new Image();
    image.src = backgroundImage;

    // Draw the background at an offset to create the looping effect
    context.drawImage(image, mode2BackgroundOffset, 0, canvas.width, canvas.height);
    context.drawImage(image, mode2BackgroundOffset + canvas.width, 0, canvas.width, canvas.height);
}


//drawing start image for game
function drawBackground(backgroundImage){
    let image = new Image();
    image.src = backgroundImage;
    context.drawImage(image, 0, 0, canvas.width, canvas.height);


}


//Game loop function 
function gameloop() {
        update();//call update function
        draw();//call draw function
        applyGravity();
      window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);


// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame